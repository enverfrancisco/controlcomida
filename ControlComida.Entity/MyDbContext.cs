﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlComida.Entity
{
    public class MyDbContext  : Entities, IDisposable
    {
        public MyDbContext()
        {
            base.Configuration.LazyLoadingEnabled = false;
        }

        public void Dispose()
        {
            base.Dispose();
        }
    }
}
