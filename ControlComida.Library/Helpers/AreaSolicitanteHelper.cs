﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlComida.Library.Helpers
{
   public static class AreaSolicitanteHelper
    {
       public static Model.AreaSolicitante ToModel(this Entity.AreaSolicitante table)
       {
           var model = new Model.AreaSolicitante
           {
               AreaId = table.Id,
               Nombre = table.DescArea
           };

           
           if (table.SolicitudComidaAdics != null)
           { 
           
           }

           return model;
       }

       public static Entity.AreaSolicitante ToTable(this Model.AreaSolicitante area)
       {
           var model = new Entity.AreaSolicitante
           {
             Id   = area.AreaId,
             DescArea = area.Nombre
           };


           //if (area.SolicitudComidaAdics != null)
           //{

           //}

           return model;
       }

       public static List<Model.AreaSolicitante> GetAllAreas()
       {
            using (var context = new Entity.MyDbContext())
            {
                List<Model.AreaSolicitante> areas = new List<Model.AreaSolicitante>();
                var result = context.AreaSolicitantes.ToList();
                if (result == null)
                {
                    return null;
                }

                foreach (var item in result)
                {
                    areas.Add(item.ToModel());
                }                                 

                return areas;
            }
       }
    }
}
