﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlComida.Library.Model
{
    public class AreaSolicitante
    {
        public int AreaId { get; set; }
        public string Nombre { get; set; }

        public List<string> PersonasNames { get; set;}
    }
}
