﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlComida.Library.Helpers;
using ControlComida.Library.Model;


namespace ControlComida.Web.Controllers
{
    public class AreaSolicitanteController : Controller
    {
        //
        // GET: /AreaSolicitante/

        public ActionResult Index()
        {
            var areas =AreaSolicitanteHelper.GetAllAreas();
            return View(areas);
        }

        [HttpPost]
        public ActionResult Index(List<AreaSolicitante> areas)
        {
            return View(areas);
        }


        //
        // GET: /AreaSolicitante/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /AreaSolicitante/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AreaSolicitante/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /AreaSolicitante/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /AreaSolicitante/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /AreaSolicitante/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /AreaSolicitante/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public PartialViewResult GetRow(AreaSolicitante area)
        {
            return PartialView("AreaSolicitanteRow", area);
        }
    }
}
