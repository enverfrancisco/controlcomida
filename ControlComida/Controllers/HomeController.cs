﻿using System.Linq;
using System.Web.Mvc;
using ControlComida.Models;
using System.Collections.Generic;

namespace ControlComida.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        /*ControlComidaEntities _db;
        
        public HomeController()
        {
            _db = new ControlComidaEntities();
        }

        public ActionResult Index()
        {
            //ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            ViewData.Model = _db.AreaSolicitante.ToList();

            return View();
        }
        */

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            using (var context = new ControlComidaEntities())
            {
                var result = context.AreaSolicitante;

                return View(result.ToList());
            }

            //List<AreaSolicitante> solicitudComida = new List<AreaSolicitante>();

          //  return View(new AreaSolicitante() { areaSolicitante = new List<AreaSolicitante>() });

            //return View(solicitudComida.ToList());
        }

        /*[HttpPost]
        public ActionResult Index(List<AreaSolicitante> solicitudComida)
        {
            return View(solicitudComida.ToList());
        }*/
        

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
