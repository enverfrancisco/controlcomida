
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/02/2015 15:03:34
-- Generated from EDMX file: C:\Users\e.francisco\documents\visual studio 2013\Projects\ControlComida\ControlComida\Models\mControlComida.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ControlComida];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_SolicitudComidaAdic_To_AreaSolicitante]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SolicitudComidaAdic] DROP CONSTRAINT [FK_SolicitudComidaAdic_To_AreaSolicitante];
GO
IF OBJECT_ID(N'[dbo].[FK_SolicitudComidaAdic_To_Comedor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SolicitudComidaAdic] DROP CONSTRAINT [FK_SolicitudComidaAdic_To_Comedor];
GO
IF OBJECT_ID(N'[dbo].[FK_SolicitudComidaAdic_To_TipoComida]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SolicitudComidaAdic] DROP CONSTRAINT [FK_SolicitudComidaAdic_To_TipoComida];
GO
IF OBJECT_ID(N'[dbo].[FK_SolicitudComidaEmp_To_AreaSolicitante]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SolicitudComidaEmp] DROP CONSTRAINT [FK_SolicitudComidaEmp_To_AreaSolicitante];
GO
IF OBJECT_ID(N'[dbo].[FK_SolicitudComidaEmp_To_Comedor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SolicitudComidaEmp] DROP CONSTRAINT [FK_SolicitudComidaEmp_To_Comedor];
GO
IF OBJECT_ID(N'[dbo].[FK_SolicitudComidaEmp_To_TipoComida]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SolicitudComidaEmp] DROP CONSTRAINT [FK_SolicitudComidaEmp_To_TipoComida];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AreaSolicitante]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AreaSolicitante];
GO
IF OBJECT_ID(N'[dbo].[Comedor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Comedor];
GO
IF OBJECT_ID(N'[dbo].[SolicitudComidaAdic]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SolicitudComidaAdic];
GO
IF OBJECT_ID(N'[dbo].[SolicitudComidaEmp]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SolicitudComidaEmp];
GO
IF OBJECT_ID(N'[dbo].[TipoComida]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipoComida];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AreaSolicitante'
CREATE TABLE [dbo].[AreaSolicitante] (
    [Id] int  NOT NULL,
    [DescArea] nchar(100)  NOT NULL
);
GO

-- Creating table 'Comedor'
CREATE TABLE [dbo].[Comedor] (
    [Id] int  NOT NULL,
    [DescComedor] nchar(15)  NOT NULL
);
GO

-- Creating table 'SolicitudComidaAdic'
CREATE TABLE [dbo].[SolicitudComidaAdic] (
    [Fecha] datetime  NOT NULL,
    [Nombre] nvarchar(120)  NOT NULL,
    [Observaciones] nchar(100)  NULL,
    [UsuarioSolicita] nchar(30)  NOT NULL,
    [UsuarioAutoriza] nchar(30)  NOT NULL,
    [UsuarioVerifica] nchar(30)  NULL,
    [AreaSolicitante] int  NOT NULL,
    [TipoComida] int  NOT NULL,
    [Comedor] int  NOT NULL
);
GO

-- Creating table 'SolicitudComidaEmp'
CREATE TABLE [dbo].[SolicitudComidaEmp] (
    [CodigoEmpleado] int  NOT NULL,
    [Fecha] datetime  NOT NULL,
    [NombreEmpleado] nvarchar(120)  NOT NULL,
    [HorarioTrabajo] binary(8)  NOT NULL,
    [Observaciones] nchar(100)  NULL,
    [UsuarioSolicita] nchar(30)  NOT NULL,
    [UsuarioAutoriza] nchar(30)  NOT NULL,
    [UsuarioVerifica] nchar(30)  NULL,
    [AreaSolicitante] int  NOT NULL,
    [TipoComida] int  NOT NULL,
    [Comedor] int  NOT NULL
);
GO

-- Creating table 'TipoComida'
CREATE TABLE [dbo].[TipoComida] (
    [Id] int  NOT NULL,
    [DescTipoComida] nchar(10)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'AreaSolicitante'
ALTER TABLE [dbo].[AreaSolicitante]
ADD CONSTRAINT [PK_AreaSolicitante]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Comedor'
ALTER TABLE [dbo].[Comedor]
ADD CONSTRAINT [PK_Comedor]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Nombre] in table 'SolicitudComidaAdic'
ALTER TABLE [dbo].[SolicitudComidaAdic]
ADD CONSTRAINT [PK_SolicitudComidaAdic]
    PRIMARY KEY CLUSTERED ([Nombre] ASC);
GO

-- Creating primary key on [CodigoEmpleado] in table 'SolicitudComidaEmp'
ALTER TABLE [dbo].[SolicitudComidaEmp]
ADD CONSTRAINT [PK_SolicitudComidaEmp]
    PRIMARY KEY CLUSTERED ([CodigoEmpleado] ASC);
GO

-- Creating primary key on [Id] in table 'TipoComida'
ALTER TABLE [dbo].[TipoComida]
ADD CONSTRAINT [PK_TipoComida]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AreaSolicitante] in table 'SolicitudComidaAdic'
ALTER TABLE [dbo].[SolicitudComidaAdic]
ADD CONSTRAINT [FK_SolicitudComidaAdic_To_AreaSolicitante]
    FOREIGN KEY ([AreaSolicitante])
    REFERENCES [dbo].[AreaSolicitante]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SolicitudComidaAdic_To_AreaSolicitante'
CREATE INDEX [IX_FK_SolicitudComidaAdic_To_AreaSolicitante]
ON [dbo].[SolicitudComidaAdic]
    ([AreaSolicitante]);
GO

-- Creating foreign key on [AreaSolicitante] in table 'SolicitudComidaEmp'
ALTER TABLE [dbo].[SolicitudComidaEmp]
ADD CONSTRAINT [FK_SolicitudComidaEmp_To_AreaSolicitante]
    FOREIGN KEY ([AreaSolicitante])
    REFERENCES [dbo].[AreaSolicitante]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SolicitudComidaEmp_To_AreaSolicitante'
CREATE INDEX [IX_FK_SolicitudComidaEmp_To_AreaSolicitante]
ON [dbo].[SolicitudComidaEmp]
    ([AreaSolicitante]);
GO

-- Creating foreign key on [Comedor] in table 'SolicitudComidaAdic'
ALTER TABLE [dbo].[SolicitudComidaAdic]
ADD CONSTRAINT [FK_SolicitudComidaAdic_To_Comedor]
    FOREIGN KEY ([Comedor])
    REFERENCES [dbo].[Comedor]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SolicitudComidaAdic_To_Comedor'
CREATE INDEX [IX_FK_SolicitudComidaAdic_To_Comedor]
ON [dbo].[SolicitudComidaAdic]
    ([Comedor]);
GO

-- Creating foreign key on [Comedor] in table 'SolicitudComidaEmp'
ALTER TABLE [dbo].[SolicitudComidaEmp]
ADD CONSTRAINT [FK_SolicitudComidaEmp_To_Comedor]
    FOREIGN KEY ([Comedor])
    REFERENCES [dbo].[Comedor]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SolicitudComidaEmp_To_Comedor'
CREATE INDEX [IX_FK_SolicitudComidaEmp_To_Comedor]
ON [dbo].[SolicitudComidaEmp]
    ([Comedor]);
GO

-- Creating foreign key on [TipoComida] in table 'SolicitudComidaAdic'
ALTER TABLE [dbo].[SolicitudComidaAdic]
ADD CONSTRAINT [FK_SolicitudComidaAdic_To_TipoComida]
    FOREIGN KEY ([TipoComida])
    REFERENCES [dbo].[TipoComida]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SolicitudComidaAdic_To_TipoComida'
CREATE INDEX [IX_FK_SolicitudComidaAdic_To_TipoComida]
ON [dbo].[SolicitudComidaAdic]
    ([TipoComida]);
GO

-- Creating foreign key on [TipoComida] in table 'SolicitudComidaEmp'
ALTER TABLE [dbo].[SolicitudComidaEmp]
ADD CONSTRAINT [FK_SolicitudComidaEmp_To_TipoComida]
    FOREIGN KEY ([TipoComida])
    REFERENCES [dbo].[TipoComida]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SolicitudComidaEmp_To_TipoComida'
CREATE INDEX [IX_FK_SolicitudComidaEmp_To_TipoComida]
ON [dbo].[SolicitudComidaEmp]
    ([TipoComida]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------